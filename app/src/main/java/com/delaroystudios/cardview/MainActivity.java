package com.delaroystudios.cardview;

import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;


public class MainActivity extends AppCompatActivity {
    GalleryFragment galleryFragment;
    FragmentTransaction fTrans;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        galleryFragment = new GalleryFragment();

        fTrans = getSupportFragmentManager().beginTransaction()
                .replace(R.id.frgmCont, galleryFragment);
        fTrans.commit();

        //писать код для галереи в GalleryFragment. MainActivity не трогайте
    }
}
